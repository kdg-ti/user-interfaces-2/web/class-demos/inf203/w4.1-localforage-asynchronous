import localforage from "localforage";

const addButton = document.getElementById("add-button");
const getButton = document.getElementById("get-button");
const getParentButton = document.getElementById("parent-button");
const personId = document.getElementById("person-id");
const personName = document.getElementById("person-name");
const parentId = document.getElementById("parent-id");
const output = document.getElementById("output");

const PERSON_KEY = "person-"

function addPerson() {
  //code zonder wachten op async funtie
  // console.log(localforage.setItem(PERSON_KEY + personId.value ,{
  //     id:personId.value,
  //     name:personName.value,
  //     parentId:parentId.value
  // }));
  localforage.setItem(PERSON_KEY + personId.value, {
    id: personId.value,
    name: personName.value,
    parentId: parentId.value
  }, (err, value) => console.log(err ?? value));
  console.log("Person added");
}

function getPerson() {
  localforage.getItem(PERSON_KEY + personId.value,
    (err, value) => output.innerText += err ?? JSON.stringify(value));
}

// the first version of getParent, where the async response is received in a callback
// nested callbacks tend to give rise to complex code, AKA Pyramid of Doom
function getParentCallback() {
  localforage.getItem(PERSON_KEY + personId.value, (err, value) => {
    if (err || !value) {
      output.innerText = "person not found"
    }
    else {
      localforage.getItem(PERSON_KEY + value.parentId, (err, value) => {
        output.innerText += err ?? JSON.stringify(value)
      })
    }
  });
}

// the 2nd version of getParent
// localforage can also return the answer in a promise
// you handle the promise repsonse in a CHAINED then call
// parameters value and err are in inverse order compared to the callback
// Chaining avoids nesting of callbacks and gives more readable code

function getParentThen() {
  localforage.getItem(PERSON_KEY + personId.value)
    .then((value, err) => {
      if (err || !value) {
        output.innerText = "person not found"
        return Promise.reject(err)
      }
      else {
        return localforage.getItem(PERSON_KEY + value.parentId);
      }
    })
    .then((value, err) => {
      output.innerText += err ?? JSON.stringify(value)
    });
}

// the 3rd version of getParent and the most common way to use function chaining
// then takes 1 parameter (value)
// errors are caught be a chained .catch (error) function
function getParentThenCatch() {
  localforage.getItem(PERSON_KEY + personId.value)
    .then(value => localforage.getItem(PERSON_KEY + value.parentId))
    .then(value => {
      output.innerText = JSON.stringify(value)
    })
    .catch(err => {
      output.innerText = "Parent not found" + err
    });
}

// The 4the version of getparent based on ASYNC/AWAIT
// This is the getparent that is currently used by the application
// The code structure using these asynchronous calls to localforage
// is very close to the structure of synchronous code
// and feels natural for many developers
// await is only allowed in async functions
// or recently also as top level code in modules
// take care in webpack you need to enable this feature as experimental
async function getParent() {
  try {
    let person = await localforage.getItem(PERSON_KEY + personId.value);
    output.innerText = JSON.stringify(await localforage.getItem(PERSON_KEY + person.parentId))
  } catch (err) {
    output.innerText = "Parent not found" + err;
  }
  
}

export function init() {
  addButton.addEventListener('click', addPerson);
  getButton.addEventListener('click', getPerson);
  getParentButton.addEventListener('click', getParent);
}
